import os
import io
import sys
import numpy as np
from mpl_toolkits.mplot3d import axes3d
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import matplotlib.pyplot as plt
from matplotlib import cm


hbarDivk = 7.634 ## (ps*K/rad)
TopW = 67.0 ## Convert unit to pW
DIM = 3

##--------This is the real data sets used in alkane Landauer------------
# INPUT = "./InputData_Gromacs/AuS_alkane_multiterm_Au1stlayer/C2/input.txt"
##-------------------------------------------------

##--------This is the real data sets used for polyyenes------------------
INPUT = "./InputData_Gromacs/AuS_conjugated_triple_multiterm/C2/input.txt"
##------------------------------------------------------------------
def import_modes(input_file=INPUT):
    temp = np.array([])
    coup = np.array([])
    modes = np.array([])
    mass = np.array([])
    anchor = np.array([], dtype=int)
    # coef = np.array(([]))
    with open(input_file,'r') as f:
        # for num, line in enumerate(f, 1):
        for num, line in enumerate(f):
            # By default, enumerate starts counting from 0,
            # if you want it to start num at 1, should use
            # enumerate(f,1)
            a = np.array(list(map(float, line.split())))
            # in python2, you don't need list function here,
            # but in python3, map returns an iterator now.
            if num == 0:
                atom_num = int(a)
                degree_of_freedom = DIM * atom_num
                # coef = np.zeros((degree_of_freedom,degree_of_freedom))
                coef = np.empty((0,degree_of_freedom))
            # num == 1 originallly is the Debye frequency which is not useful
            # in our current calculation
            # We may convert it to mass line, which can be used for
            # mass-weighted orthogonal coefficients
            if num == 1:
                mass = np.append(mass,a)
            if num == 2:
                temp = np.append(temp,a)
            if num == 3:
                coup = np.append(coup,a)
            if num == 4:
                modes = np.append(modes,a)
            if num > 4 and num < (5+DIM*atom_num):
                coef = np.append(coef,[a], axis=0)
            if num > (4+DIM*atom_num):
                anchor = np.append(anchor,int(a))
    mass = np.repeat(mass,DIM)
    mass_mat =  np.sqrt(mass) * np.eye(degree_of_freedom)
    coef_mass = np.dot(mass_mat,np.transpose(coef))

    return atom_num, temp, coup, modes, coef_mass, anchor

def calc_Sigma(omega,gammaL_mtx,gammaR_mtx,coeff_C):
    DegOFree = coeff_C.shape[0]
    Sigma_d = np.zeros((DegOFree,DegOFree),dtype=np.complex_)
    # Sigma_d[0,0] = 1j*gammaL*omega
    # Sigma_d[DegOFree-1,DegOFree-1] = 1j*gammaR*omega
    Sigma_d = 1j*(gammaL_mtx + gammaR_mtx)
    # return np.dot(np.dot(np.linalg.inv(coeff_C),Sigma_d),coeff_C)
    return Sigma_d

def calc_Gamma(omega,gammaL,gammaR,coeff_C,anchor):
    anchor_len = len(anchor)
    DegOFree = coeff_C.shape
    # gammaL_d = np.zeros((DegOFree,DegOFree))
    # gammaR_d = np.zeros((DegOFree,DegOFree))
    gammaL_d = np.zeros(DegOFree)
    gammaR_d = np.zeros(DegOFree)
    omegaD = 50 ## 37.67 rad/ps is 200cm^-1
    for l, value in enumerate(anchor):
        if l < anchor_len//2:
            for i in range(DIM):
                # gammaL_d[i+(value-1)*DIM,i+(value-1)*DIM] = gammaL
                gammaL_d[i+(value-1)*DIM,i+(value-1)*DIM] = omega*gammaL
                # gammaL_d[i+(value-1)*DIM,i+(value-1)*DIM] = omega*gammaL*np.exp(-omega/omegaD)
        else:
            for i in range(DIM):
                # gammaR_d[i+(value-1)*DIM,i+(value-1)*DIM] = gammaR
                gammaR_d[i+(value-1)*DIM,i+(value-1)*DIM] = omega*gammaR
                # gammaR_d[i+(value-1)*DIM,i+(value-1)*DIM] = omega*gammaR*np.exp(-omega/omegaD)
    coeff_inv = np.linalg.inv(coeff_C)
    return np.dot(np.dot(coeff_inv,gammaL_d),coeff_C),np.dot(np.dot(coeff_inv,gammaR_d),coeff_C)

def calc_Green(omega,omega_mode,Sigma):
    DegOFree = len(omega_mode)
    omega_identity = np.eye(DegOFree)*omega**2
    Phi = np.eye(DegOFree) * (omega_mode**2)
    return np.linalg.inv(-omega_identity+Phi-Sigma)

def calc_transmission(green, Gamma_mode_L, Gamma_mode_R):
    green_dagger = np.matrix.getH(green) #Hermitian conjugation
    matrix_multi = np.dot(np.dot(green,Gamma_mode_L), np.dot(green_dagger,Gamma_mode_R))
    return np.trace(matrix_multi)

def calc_pop(omega, T):
    return 1 / (np.exp(hbarDivk * omega / T)-1)

def calc_pop_de(omega, T):
    return hbarDivk * omega * np.exp(hbarDivk * omega / T) / (T**2 *
            (np.exp(hbarDivk * omega / T)-1)**2)

def calc_conductance_linear(trans, omega_array, pop_derivative):
    d_omega = omega_array[2] - omega_array[1]
    int_elements = trans * omega_array * pop_derivative
    return  67 * np.sum(int_elements) * d_omega # turn the unit to pW/K

def calc_current_quantum(trans, omega_array, pop_difference):
    d_omega = omega_array[2] - omega_array[1]
    int_elements = trans * omega_array * pop_difference
    # return  np.sum(int_elements) * d_omega / (2 * np.pi)
    return  np.sum(int_elements) * d_omega 

def calc_current_classical(trans, omega_array, DT):
    d_omega = omega_array[2] - omega_array[1]
    # return DT * np.sum(trans) * d_omega / (2 * np.pi * hbarDivk)
    return DT * np.sum(trans) * d_omega /  hbarDivk

def plot_mode_localization(n, coef):
    ## n is the nth mode number
    atom_num = coef.shape[0] // DIM
    y = np.zeros(atom_num)
    for l in range(atom_num):
        for i in range(DIM):
            y[l] += coef[l*3+i,n-1]**2
    plt.plot(y)
    plt.show()

def plot_mode_localization_surface(eigenf, coef):
    atom_num = coef.shape[0] // DIM
    Z = np.zeros((atom_num,coef.shape[0]))
    for l in range(atom_num):
        for i in range(DIM):
            Z[l,:] += coef[l*3+i,:]**2
    atom_array = np.array(range(1,atom_num+1))

    X, Y = np.meshgrid(eigenf, atom_array)
    fig = plt.figure()
    ax = fig.gca(projection='3d')
    surf = ax.plot_surface(X, Y, Z, rstride=5, cstride=5, alpha=0.4)
    cset = ax.contour(X, Y, Z, zdir='x', offset=-50, cmap=cm.coolwarm)
    # ax.set_xlim(-50, 230)
    # ax.set_ylim(-40, 40)
    # ax.set_zlim(-0.05, 0.45)
    ax.set_xlabel('X')
    ax.set_ylabel('Y')
    ax.set_zlabel('Z')

    plt.show()


if __name__ == "__main__":

    d_omega = 0.01
    # omega = np.arange(1E-10, 37.6,  d_omega) #give the omega limit = 200cm^-1
    omega = np.arange(1E-10, 564,  d_omega) #give the omega limit = 3000cm^-1
    N = len(omega)
    gammaL = 1.
    gammaR = 1.
    TL = 300.
    TR = 350.

    AN, Tlr, Clr, omega_mode, CCoeff, anchor_idx = import_modes()

    # print (np.dot(np.transpose(CCoeff),CCoeff)) # Check if the coefficient is orthonormal
    # plot_mode_localization(3, CCoeff)
    # plot_mode_localization_surface(omega_mode, CCoeff)


    trans_array = np.zeros(N)
    pop_diff = np.zeros(N)
    pop_deriv = np.zeros(N)
    for i, value in enumerate(omega):
        Gamma_mode_L, Gamma_mode_R = calc_Gamma(value, gammaL, gammaR, CCoeff,
                anchor_idx)
        Sigma = calc_Sigma(value,Gamma_mode_L,Gamma_mode_R,CCoeff)
        Green = calc_Green(value, omega_mode, Sigma)
        transmission = calc_transmission(Green, Gamma_mode_L, Gamma_mode_R)
        trans_array[i] = transmission
        pop_diff[i] = calc_pop(value,TR)-calc_pop(value,TL)
        pop_deriv[i] = calc_pop_de(value, TL)
        # pop_deriv[i] = calc_pop_de(value, TR)
        if i % 100 == 0:
            print(i)

    trans_arrayWT = trans_array * pop_diff
    trans_arrayWTO = trans_array * pop_diff * omega

    curr1 = calc_current_classical(trans_array, omega, (TR-TL)) 
    curr2 = calc_current_quantum(trans_array, omega, pop_diff)
    cond_linear = calc_conductance_linear(trans_array, omega, pop_deriv)
    curr1 = curr1 * TopW
    curr2 = curr2 * TopW
    print ('average:', curr2/(TR-TL))
    print ('linear:', cond_linear)


    # plt.plot(omega,trans_array)

    # plt.show()

    # np.savetxt('trans_C4_multi_triple_3000.txt',np.c_[omega/0.188365,trans_array])

